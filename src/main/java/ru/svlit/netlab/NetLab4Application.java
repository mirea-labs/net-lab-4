package ru.svlit.netlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetLab4Application {

	public static void main(String[] args) {
		SpringApplication.run(NetLab4Application.class, args);
	}

}
