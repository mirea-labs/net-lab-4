package ru.svlit.netlab.service

import org.springframework.stereotype.Service
import ru.svlit.netlab.model.RandomEntry

@Service
class RandomService {

    fun random(min: Int?, max: Int?): RandomEntry {
        val minActual = min ?: MIN
        val maxActual = max ?: MAX
        return RandomEntry(minActual, maxActual)
    }
}

private const val MIN = 0
private const val MAX = 100