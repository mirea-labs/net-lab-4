package ru.svlit.netlab.model

import java.lang.System.currentTimeMillis
import kotlin.random.Random

data class RandomEntry(
        val min: Int,
        val max: Int,
        val actual: Int = RANDOM.nextInt(min, max + 1)
)

private val RANDOM = Random(currentTimeMillis())