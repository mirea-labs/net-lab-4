package ru.svlit.netlab.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.svlit.netlab.service.RandomService

@RestController
class RandomController {

    @Autowired
    private lateinit var service: RandomService

    @RequestMapping("/random")
    fun randomMinMax(@RequestParam min: Int?, @RequestParam max: Int?) = ResponseEntity(service.random(min, max), HttpStatus.OK)
}